// get-categories
$.ajax('/categories', {
    contentType: 'application/json',
    dataType: 'json',
    success: function (response) {
        var categoryElements = $.map(response, function (category, i) {
            var categoryItem = $('<li class="category-item" data-category="' + category.type + '"></li>');
            $('<a href="#" class="category-link">' + category.name + '</h3>').appendTo(categoryItem);
            return categoryItem;
        });

        // add elements to the corresponding layout areas
        $(categoryElements).each(function (index, element) {
            $('.category-list').append(this);
        });
    }
});