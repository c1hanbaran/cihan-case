var express = require('express');
var router = express.Router();

var categories = [{
        "id": 1,
        "name": "Kategori-1",
        "type": "category-1"
    },
    {
        "id": 2,
        "name": "Kategori-2",
        "type": "category-2"
    },
    {
        "id": 3,
        "name": "Kategori-3",
        "type": "category-3"
    },
    {
        "id": 4,
        "name": "Kategori-4",
        "type": "category-4"
    }
];

router.route('/')
    .get(function (request, response) {
        response.json(categories);
    });

module.exports = router;