$(document).ready(function () {
    $('.category-list').on('click', '.category-link', function(event) {
        event.preventDefault();
        var selCatLinkEl = $(this);
        var selCatName = $(this).closest('.category-item').data("category");
        $('.box-product').hide();
        $('.category-link').removeClass('category-active');
        $('.box-product').each(function(index, element) {
            if (selCatName === 'category-all') {
                $('.box-product').show();
                selCatLinkEl.addClass('category-active');
            }
            else if (selCatName === $(this).data('category')) {
                $(this).show();
                selCatLinkEl.addClass('category-active');
            }
        });
    });
});