$(document).ready(function () {
    countSubItems();
    $(".menu").find(".menu-link").on("click", function (event) {
        event.preventDefault();
        numberOfSubmenu = $(this).closest(".menu-item").find(".submenu-list").length;
        if (numberOfSubmenu > 0) {
            closeSubMenus();
            toggleSubMenu(this);
        }
    });
    $(".submenu-link").on("click", function(event) {
        event.preventDefault();
    });
});

var numberOfSubmenu;
var subMenuIsActive;


// closeSubMenus
function closeSubMenus() {
    $(".submenu-list").slideUp();
    $(".menu-link").removeClass("menu-link-active");
}

// openSubMenu
function toggleSubMenu(el) {
    var subMenuList = $(el).closest(".menu-item").find(".submenu-list");
    subMenuList.stop().slideToggle();
    $(el).toggleClass("menu-link-active");
}

// countSubItems
function countSubItems() {
    $(".menu-item").each(function () {
        if ($(this).find(".submenu-list").length > 0) {
            var numberOfSubItems = $(this).find(".submenu-item").length;
            var subCountEl = $("<span class='menu-link-subcount'></span>");
            subCountEl.text(numberOfSubItems);
            $(this).find(".menu-link").append(subCountEl);
        }
    });
}