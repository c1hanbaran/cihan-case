// for development code run 'gulp'
// for production code run 'gulp --type prod'
gulp = require('gulp'),
sass = require('gulp-sass'),
autoprefixer = require('gulp-autoprefixer'),
sourcemaps = require('gulp-sourcemaps'),
gutil = require('gulp-util'),
concat = require('gulp-concat'),
cache = require('gulp-cache'),
uglify = require('gulp-uglify');

var folder = {};

// src file path
folder['src'] = './src';
folder['sass_src'] = folder['src'] + '/sass';
folder['js_src'] = folder['src'] + '/js';

// dist file path
folder['dist'] = '../dist';
folder['assets'] = folder['dist'] + '/assets';
folder['css'] = folder['assets'] + '/css';
folder['js'] = folder['assets'] + '/js';

// styleguide file path
folder['styleguide'] = '../styleguide';
folder['styleguide-src'] = folder['styleguide'] + '/src';
folder['styleguide-assets'] = folder['styleguide'] + '/assets';
folder['styleguide-css'] = folder['styleguide-assets'] + '/css';
folder['styleguide-js'] = folder['styleguide-assets'] + '/js';

// packages
var VendorArray = {
	'jsVendor': [
		// jquery
		'node_modules/jquery/dist/jquery.min.js'
	]
};

// css tasks
gulp.task('build-sass', function () {
	return gulp.src(folder['sass_src'] + '/**/*.scss')
		.pipe(gutil.env.type === 'prod' ? gutil.noop() : sourcemaps.init())
		.pipe(gutil.env.type === 'prod' ? sass({
			outputStyle: 'compressed'
		}).on('error', sass.logError) : sass({
			outputStyle: 'expanded'
		}).on('error', sass.logError))
		.pipe(autoprefixer('last 2 version'))
		.pipe(gutil.env.type === 'prod' ? gutil.noop() : sourcemaps.write())
		.pipe(gulp.dest(folder['css']));
});

// js tasks
gulp.task('build-js', function () {
	return gulp.src(folder['js_src'] + '/**/*.js')
		.pipe(gutil.env.type === 'prod' ? gutil.noop() : sourcemaps.init())
		.pipe(concat('main.js'))
		.pipe(gutil.env.type === 'prod' ? uglify() : gutil.noop())
		.pipe(gutil.env.type === 'prod' ? gutil.noop() : sourcemaps.write())
		.pipe(gulp.dest(folder['js']));
});

gulp.task('jsvendor', function () {
	return gulp.src(VendorArray['jsVendor'])
		.pipe(gutil.env.type === 'prod' ? gutil.noop() : sourcemaps.init())
		.pipe(concat('vendor.js'))
		.pipe(gutil.env.type === 'prod' ? uglify() : gutil.noop())
		.pipe(gutil.env.type === 'prod' ? gutil.noop() : sourcemaps.write())
		.pipe(gulp.dest(folder['js']))
});

// styleguide tasks
gulp.task('styleguide-js', function () {
	return gulp.src(folder['styleguide-src'] + '/**/*.js')
		.pipe(gutil.env.type === 'prod' ? gutil.noop() : sourcemaps.init())
		.pipe(concat('main.js'))
		.pipe(gutil.env.type === 'prod' ? uglify() : gutil.noop())
		.pipe(gutil.env.type === 'prod' ? gutil.noop() : sourcemaps.write())
		.pipe(uglify())
		.pipe(gulp.dest(folder['styleguide-js']));
});

gulp.task('styleguide-sass', function () {
	return gulp.src(folder['styleguide-src'] + '/sass/**/*.scss')
		.pipe(gutil.env.type === 'prod' ? gutil.noop() : sourcemaps.init())
		.pipe(sass({
			outputStyle: 'compressed'
		}).on('error', sass.logError))
		.pipe(autoprefixer('last 2 version'))
		.pipe(gutil.env.type === 'prod' ? gutil.noop() : sourcemaps.write())
		.pipe(gulp.dest(folder['styleguide-css']));
});

// default task
gulp.task('default', ['build-sass', 'build-js', 'jsvendor', 'styleguide-js', 'styleguide-sass'], function () {
	gulp.watch(folder['sass_src'] + '/**/*.scss', ['build-sass']);
	gulp.watch(folder['js_src'] + '/**/*.js', ['build-js']);
	gulp.watch(folder['styleguide-src'] + '/**/*.js', ['styleguide-js']);
	gulp.watch(folder['styleguide-src'] + '/**/*.scss', ['styleguide-sass']);
});