// get-campaigns
$.ajax('/campaigns', {
    contentType: 'application/json',
    dataType: 'json',
    success: function (response) {
        var campaignElements = $.map(response, function (campaign, i) {
            var campaignItem = $('<div class="box box-campaign"></div>');
            $('<h3>' + campaign.name + '</h3>').appendTo(campaignItem);
            return campaignItem;
        });

        // add elements to the corresponding layout areas
        $(".box-container-7").append(campaignElements[0]);
        $(".box-container-8").append(campaignElements[1]);
    }
});