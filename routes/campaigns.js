var express = require('express');
var router = express.Router();

var campaigns = [{
        "id": 1,
        "name": "Kampanya 1"
    },
    {
        "id": 2,
        "name": "Kampanya 2"
    }
];

router.route('/')
    .get(function (request, response) {
        response.json(campaigns);
    });

module.exports = router;