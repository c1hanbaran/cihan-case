var express = require('express');
var app = express();

app.use(express.static('public'));

var boutiques = require('./routes/boutiques');
var categories = require('./routes/categories');
var campaigns = require('./routes/campaigns');

app.use('/boutiques', boutiques);
app.use('/categories', categories);
app.use('/campaigns', campaigns);

app.listen(3000, function() {
    console.log('Listening on port 3000');
});