$(document).ready(function () {
    $('.category-list').on('click', '.category-link', function(event) {
        event.preventDefault();
        var selCatLinkEl = $(this);
        var selCatName = $(this).closest('.category-item').data("category");
        $('.box-product').hide();
        $('.category-link').removeClass('category-active');
        $('.box-product').each(function(index, element) {
            if (selCatName === 'category-all') {
                $('.box-product').show();
                selCatLinkEl.addClass('category-active');
            }
            else if (selCatName === $(this).data('category')) {
                $(this).show();
                selCatLinkEl.addClass('category-active');
            }
        });
    });
});
// get-boutique
var CountDownDateFromServer;
$.ajax('/boutiques', {
    contentType: 'application/json',
    dataType: 'json',
    success: function(response) {
        var boutiqueElements = $.map(response, function(boutique, i){
            var boutiqueItem = $('<div class="box box-product" data-category="'+boutique.category +'"></div>');
            $('<h3>'+boutique.name+'</h3>').appendTo(boutiqueItem);
            $('<img>').attr('src', boutique.img).appendTo(boutiqueItem);
            $('<p class="expire-date">'+boutique.date+'</p>').appendTo(boutiqueItem);
            CountDownDateFromServer = boutique.date;
            return boutiqueItem;
        });

        // add elements to the corresponding layout areas
        $(".box-container-1").append(boutiqueElements[0]);
        $(".box-container-2").append(boutiqueElements[1]);
        $(".box-container-3").append(boutiqueElements[2]);
        $(".box-container-4").append(boutiqueElements[3]);
        $(".box-container-5").append(boutiqueElements[4]);
        $(".box-container-6").append(boutiqueElements[5]);
    }
});



// get-campaigns
$.ajax('/campaigns', {
    contentType: 'application/json',
    dataType: 'json',
    success: function (response) {
        var campaignElements = $.map(response, function (campaign, i) {
            var campaignItem = $('<div class="box box-campaign"></div>');
            $('<h3>' + campaign.name + '</h3>').appendTo(campaignItem);
            return campaignItem;
        });

        // add elements to the corresponding layout areas
        $(".box-container-7").append(campaignElements[0]);
        $(".box-container-8").append(campaignElements[1]);
    }
});
// get-categories
$.ajax('/categories', {
    contentType: 'application/json',
    dataType: 'json',
    success: function (response) {
        var categoryElements = $.map(response, function (category, i) {
            var categoryItem = $('<li class="category-item" data-category="' + category.type + '"></li>');
            $('<a href="#" class="category-link">' + category.name + '</h3>').appendTo(categoryItem);
            return categoryItem;
        });

        // add elements to the corresponding layout areas
        $(categoryElements).each(function (index, element) {
            $('.category-list').append(this);
        });
    }
});
'use strict';

// count-down
$(document).ready(function(){
    console.log(CountDownDateFromServer);
    var countDownDate = new Date(CountDownDateFromServer).getTime();
    var x = setInterval(function () {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        $('.box-product').find(".expire-date").text(days + "days " + hours + "hours " + minutes + "minutes " + seconds + "seconds");

        if (distance < 0) {
            clearInterval(x);
            $('.box-product').find(".expire-date").text("Expired!");
        }
    }, 1000);
});
$(document).ready(function () {
    countSubItems();
    $(".menu").find(".menu-link").on("click", function (event) {
        event.preventDefault();
        numberOfSubmenu = $(this).closest(".menu-item").find(".submenu-list").length;
        if (numberOfSubmenu > 0) {
            closeSubMenus();
            toggleSubMenu(this);
        }
    });
    $(".submenu-link").on("click", function(event) {
        event.preventDefault();
    });
});

var numberOfSubmenu;
var subMenuIsActive;


// closeSubMenus
function closeSubMenus() {
    $(".submenu-list").slideUp();
    $(".menu-link").removeClass("menu-link-active");
}

// openSubMenu
function toggleSubMenu(el) {
    var subMenuList = $(el).closest(".menu-item").find(".submenu-list");
    subMenuList.stop().slideToggle();
    $(el).toggleClass("menu-link-active");
}

// countSubItems
function countSubItems() {
    $(".menu-item").each(function () {
        if ($(this).find(".submenu-list").length > 0) {
            var numberOfSubItems = $(this).find(".submenu-item").length;
            var subCountEl = $("<span class='menu-link-subcount'></span>");
            subCountEl.text(numberOfSubItems);
            $(this).find(".menu-link").append(subCountEl);
        }
    });
}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJvdXRpcXVlLWZpbHRlci5qcyIsImJvdXRpcXVlcy5qcyIsImNhbXBhaWducy5qcyIsImNhdGVnb3JpZXMuanMiLCJtYWluLmpzIiwiY29tcG9uZW50cy9jb3VudC1kb3duLmpzIiwiY29tcG9uZW50cy9tZW51LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDbEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNoQkE7QUFDQTtBQ0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNuQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoibWFpbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgICAkKCcuY2F0ZWdvcnktbGlzdCcpLm9uKCdjbGljaycsICcuY2F0ZWdvcnktbGluaycsIGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHZhciBzZWxDYXRMaW5rRWwgPSAkKHRoaXMpO1xuICAgICAgICB2YXIgc2VsQ2F0TmFtZSA9ICQodGhpcykuY2xvc2VzdCgnLmNhdGVnb3J5LWl0ZW0nKS5kYXRhKFwiY2F0ZWdvcnlcIik7XG4gICAgICAgICQoJy5ib3gtcHJvZHVjdCcpLmhpZGUoKTtcbiAgICAgICAgJCgnLmNhdGVnb3J5LWxpbmsnKS5yZW1vdmVDbGFzcygnY2F0ZWdvcnktYWN0aXZlJyk7XG4gICAgICAgICQoJy5ib3gtcHJvZHVjdCcpLmVhY2goZnVuY3Rpb24oaW5kZXgsIGVsZW1lbnQpIHtcbiAgICAgICAgICAgIGlmIChzZWxDYXROYW1lID09PSAnY2F0ZWdvcnktYWxsJykge1xuICAgICAgICAgICAgICAgICQoJy5ib3gtcHJvZHVjdCcpLnNob3coKTtcbiAgICAgICAgICAgICAgICBzZWxDYXRMaW5rRWwuYWRkQ2xhc3MoJ2NhdGVnb3J5LWFjdGl2ZScpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAoc2VsQ2F0TmFtZSA9PT0gJCh0aGlzKS5kYXRhKCdjYXRlZ29yeScpKSB7XG4gICAgICAgICAgICAgICAgJCh0aGlzKS5zaG93KCk7XG4gICAgICAgICAgICAgICAgc2VsQ2F0TGlua0VsLmFkZENsYXNzKCdjYXRlZ29yeS1hY3RpdmUnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfSk7XG59KTsiLCIvLyBnZXQtYm91dGlxdWVcbnZhciBDb3VudERvd25EYXRlRnJvbVNlcnZlcjtcbiQuYWpheCgnL2JvdXRpcXVlcycsIHtcbiAgICBjb250ZW50VHlwZTogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgIGRhdGFUeXBlOiAnanNvbicsXG4gICAgc3VjY2VzczogZnVuY3Rpb24ocmVzcG9uc2UpIHtcbiAgICAgICAgdmFyIGJvdXRpcXVlRWxlbWVudHMgPSAkLm1hcChyZXNwb25zZSwgZnVuY3Rpb24oYm91dGlxdWUsIGkpe1xuICAgICAgICAgICAgdmFyIGJvdXRpcXVlSXRlbSA9ICQoJzxkaXYgY2xhc3M9XCJib3ggYm94LXByb2R1Y3RcIiBkYXRhLWNhdGVnb3J5PVwiJytib3V0aXF1ZS5jYXRlZ29yeSArJ1wiPjwvZGl2PicpO1xuICAgICAgICAgICAgJCgnPGgzPicrYm91dGlxdWUubmFtZSsnPC9oMz4nKS5hcHBlbmRUbyhib3V0aXF1ZUl0ZW0pO1xuICAgICAgICAgICAgJCgnPGltZz4nKS5hdHRyKCdzcmMnLCBib3V0aXF1ZS5pbWcpLmFwcGVuZFRvKGJvdXRpcXVlSXRlbSk7XG4gICAgICAgICAgICAkKCc8cCBjbGFzcz1cImV4cGlyZS1kYXRlXCI+Jytib3V0aXF1ZS5kYXRlKyc8L3A+JykuYXBwZW5kVG8oYm91dGlxdWVJdGVtKTtcbiAgICAgICAgICAgIENvdW50RG93bkRhdGVGcm9tU2VydmVyID0gYm91dGlxdWUuZGF0ZTtcbiAgICAgICAgICAgIHJldHVybiBib3V0aXF1ZUl0ZW07XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vIGFkZCBlbGVtZW50cyB0byB0aGUgY29ycmVzcG9uZGluZyBsYXlvdXQgYXJlYXNcbiAgICAgICAgJChcIi5ib3gtY29udGFpbmVyLTFcIikuYXBwZW5kKGJvdXRpcXVlRWxlbWVudHNbMF0pO1xuICAgICAgICAkKFwiLmJveC1jb250YWluZXItMlwiKS5hcHBlbmQoYm91dGlxdWVFbGVtZW50c1sxXSk7XG4gICAgICAgICQoXCIuYm94LWNvbnRhaW5lci0zXCIpLmFwcGVuZChib3V0aXF1ZUVsZW1lbnRzWzJdKTtcbiAgICAgICAgJChcIi5ib3gtY29udGFpbmVyLTRcIikuYXBwZW5kKGJvdXRpcXVlRWxlbWVudHNbM10pO1xuICAgICAgICAkKFwiLmJveC1jb250YWluZXItNVwiKS5hcHBlbmQoYm91dGlxdWVFbGVtZW50c1s0XSk7XG4gICAgICAgICQoXCIuYm94LWNvbnRhaW5lci02XCIpLmFwcGVuZChib3V0aXF1ZUVsZW1lbnRzWzVdKTtcbiAgICB9XG59KTtcblxuXG4iLCIvLyBnZXQtY2FtcGFpZ25zXG4kLmFqYXgoJy9jYW1wYWlnbnMnLCB7XG4gICAgY29udGVudFR5cGU6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICBkYXRhVHlwZTogJ2pzb24nLFxuICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICB2YXIgY2FtcGFpZ25FbGVtZW50cyA9ICQubWFwKHJlc3BvbnNlLCBmdW5jdGlvbiAoY2FtcGFpZ24sIGkpIHtcbiAgICAgICAgICAgIHZhciBjYW1wYWlnbkl0ZW0gPSAkKCc8ZGl2IGNsYXNzPVwiYm94IGJveC1jYW1wYWlnblwiPjwvZGl2PicpO1xuICAgICAgICAgICAgJCgnPGgzPicgKyBjYW1wYWlnbi5uYW1lICsgJzwvaDM+JykuYXBwZW5kVG8oY2FtcGFpZ25JdGVtKTtcbiAgICAgICAgICAgIHJldHVybiBjYW1wYWlnbkl0ZW07XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vIGFkZCBlbGVtZW50cyB0byB0aGUgY29ycmVzcG9uZGluZyBsYXlvdXQgYXJlYXNcbiAgICAgICAgJChcIi5ib3gtY29udGFpbmVyLTdcIikuYXBwZW5kKGNhbXBhaWduRWxlbWVudHNbMF0pO1xuICAgICAgICAkKFwiLmJveC1jb250YWluZXItOFwiKS5hcHBlbmQoY2FtcGFpZ25FbGVtZW50c1sxXSk7XG4gICAgfVxufSk7IiwiLy8gZ2V0LWNhdGVnb3JpZXNcbiQuYWpheCgnL2NhdGVnb3JpZXMnLCB7XG4gICAgY29udGVudFR5cGU6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICBkYXRhVHlwZTogJ2pzb24nLFxuICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICB2YXIgY2F0ZWdvcnlFbGVtZW50cyA9ICQubWFwKHJlc3BvbnNlLCBmdW5jdGlvbiAoY2F0ZWdvcnksIGkpIHtcbiAgICAgICAgICAgIHZhciBjYXRlZ29yeUl0ZW0gPSAkKCc8bGkgY2xhc3M9XCJjYXRlZ29yeS1pdGVtXCIgZGF0YS1jYXRlZ29yeT1cIicgKyBjYXRlZ29yeS50eXBlICsgJ1wiPjwvbGk+Jyk7XG4gICAgICAgICAgICAkKCc8YSBocmVmPVwiI1wiIGNsYXNzPVwiY2F0ZWdvcnktbGlua1wiPicgKyBjYXRlZ29yeS5uYW1lICsgJzwvaDM+JykuYXBwZW5kVG8oY2F0ZWdvcnlJdGVtKTtcbiAgICAgICAgICAgIHJldHVybiBjYXRlZ29yeUl0ZW07XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vIGFkZCBlbGVtZW50cyB0byB0aGUgY29ycmVzcG9uZGluZyBsYXlvdXQgYXJlYXNcbiAgICAgICAgJChjYXRlZ29yeUVsZW1lbnRzKS5lYWNoKGZ1bmN0aW9uIChpbmRleCwgZWxlbWVudCkge1xuICAgICAgICAgICAgJCgnLmNhdGVnb3J5LWxpc3QnKS5hcHBlbmQodGhpcyk7XG4gICAgICAgIH0pO1xuICAgIH1cbn0pOyIsIid1c2Ugc3RyaWN0JztcbiIsIi8vIGNvdW50LWRvd25cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7XG4gICAgY29uc29sZS5sb2coQ291bnREb3duRGF0ZUZyb21TZXJ2ZXIpO1xuICAgIHZhciBjb3VudERvd25EYXRlID0gbmV3IERhdGUoQ291bnREb3duRGF0ZUZyb21TZXJ2ZXIpLmdldFRpbWUoKTtcbiAgICB2YXIgeCA9IHNldEludGVydmFsKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIG5vdyA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xuICAgICAgICB2YXIgZGlzdGFuY2UgPSBjb3VudERvd25EYXRlIC0gbm93O1xuICAgICAgICB2YXIgZGF5cyA9IE1hdGguZmxvb3IoZGlzdGFuY2UgLyAoMTAwMCAqIDYwICogNjAgKiAyNCkpO1xuICAgICAgICB2YXIgaG91cnMgPSBNYXRoLmZsb29yKChkaXN0YW5jZSAlICgxMDAwICogNjAgKiA2MCAqIDI0KSkgLyAoMTAwMCAqIDYwICogNjApKTtcbiAgICAgICAgdmFyIG1pbnV0ZXMgPSBNYXRoLmZsb29yKChkaXN0YW5jZSAlICgxMDAwICogNjAgKiA2MCkpIC8gKDEwMDAgKiA2MCkpO1xuICAgICAgICB2YXIgc2Vjb25kcyA9IE1hdGguZmxvb3IoKGRpc3RhbmNlICUgKDEwMDAgKiA2MCkpIC8gMTAwMCk7XG5cbiAgICAgICAgJCgnLmJveC1wcm9kdWN0JykuZmluZChcIi5leHBpcmUtZGF0ZVwiKS50ZXh0KGRheXMgKyBcImRheXMgXCIgKyBob3VycyArIFwiaG91cnMgXCIgKyBtaW51dGVzICsgXCJtaW51dGVzIFwiICsgc2Vjb25kcyArIFwic2Vjb25kc1wiKTtcblxuICAgICAgICBpZiAoZGlzdGFuY2UgPCAwKSB7XG4gICAgICAgICAgICBjbGVhckludGVydmFsKHgpO1xuICAgICAgICAgICAgJCgnLmJveC1wcm9kdWN0JykuZmluZChcIi5leHBpcmUtZGF0ZVwiKS50ZXh0KFwiRXhwaXJlZCFcIik7XG4gICAgICAgIH1cbiAgICB9LCAxMDAwKTtcbn0pOyIsIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgICBjb3VudFN1Ykl0ZW1zKCk7XG4gICAgJChcIi5tZW51XCIpLmZpbmQoXCIubWVudS1saW5rXCIpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIG51bWJlck9mU3VibWVudSA9ICQodGhpcykuY2xvc2VzdChcIi5tZW51LWl0ZW1cIikuZmluZChcIi5zdWJtZW51LWxpc3RcIikubGVuZ3RoO1xuICAgICAgICBpZiAobnVtYmVyT2ZTdWJtZW51ID4gMCkge1xuICAgICAgICAgICAgY2xvc2VTdWJNZW51cygpO1xuICAgICAgICAgICAgdG9nZ2xlU3ViTWVudSh0aGlzKTtcbiAgICAgICAgfVxuICAgIH0pO1xuICAgICQoXCIuc3VibWVudS1saW5rXCIpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICB9KTtcbn0pO1xuXG52YXIgbnVtYmVyT2ZTdWJtZW51O1xudmFyIHN1Yk1lbnVJc0FjdGl2ZTtcblxuXG4vLyBjbG9zZVN1Yk1lbnVzXG5mdW5jdGlvbiBjbG9zZVN1Yk1lbnVzKCkge1xuICAgICQoXCIuc3VibWVudS1saXN0XCIpLnNsaWRlVXAoKTtcbiAgICAkKFwiLm1lbnUtbGlua1wiKS5yZW1vdmVDbGFzcyhcIm1lbnUtbGluay1hY3RpdmVcIik7XG59XG5cbi8vIG9wZW5TdWJNZW51XG5mdW5jdGlvbiB0b2dnbGVTdWJNZW51KGVsKSB7XG4gICAgdmFyIHN1Yk1lbnVMaXN0ID0gJChlbCkuY2xvc2VzdChcIi5tZW51LWl0ZW1cIikuZmluZChcIi5zdWJtZW51LWxpc3RcIik7XG4gICAgc3ViTWVudUxpc3Quc3RvcCgpLnNsaWRlVG9nZ2xlKCk7XG4gICAgJChlbCkudG9nZ2xlQ2xhc3MoXCJtZW51LWxpbmstYWN0aXZlXCIpO1xufVxuXG4vLyBjb3VudFN1Ykl0ZW1zXG5mdW5jdGlvbiBjb3VudFN1Ykl0ZW1zKCkge1xuICAgICQoXCIubWVudS1pdGVtXCIpLmVhY2goZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoJCh0aGlzKS5maW5kKFwiLnN1Ym1lbnUtbGlzdFwiKS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICB2YXIgbnVtYmVyT2ZTdWJJdGVtcyA9ICQodGhpcykuZmluZChcIi5zdWJtZW51LWl0ZW1cIikubGVuZ3RoO1xuICAgICAgICAgICAgdmFyIHN1YkNvdW50RWwgPSAkKFwiPHNwYW4gY2xhc3M9J21lbnUtbGluay1zdWJjb3VudCc+PC9zcGFuPlwiKTtcbiAgICAgICAgICAgIHN1YkNvdW50RWwudGV4dChudW1iZXJPZlN1Ykl0ZW1zKTtcbiAgICAgICAgICAgICQodGhpcykuZmluZChcIi5tZW51LWxpbmtcIikuYXBwZW5kKHN1YkNvdW50RWwpO1xuICAgICAgICB9XG4gICAgfSk7XG59Il19
