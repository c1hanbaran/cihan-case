var express = require('express');
var router = express.Router();

var boutiques = [{
        "id": 1,
        "img": "http://via.placeholder.com/120x120",
        "category": "category-1",
        "name": "butik 1",
        "date": "Sep 4, 2018 15:37:25"
    },
    {
        "id": 2,
        "img": "http://via.placeholder.com/120x120",
        "category": "category-2",
        "name": "butik 2",
        "date": "Sep 4, 2018 15:37:25"
    },
    {
        "id": 3,
        "img": "http://via.placeholder.com/120x120",
        "category": "category-3",
        "name": "butik 3",
        "date": "Sep 4, 2018 15:37:25"
    },
    {
        "id": 4,
        "img": "http://via.placeholder.com/120x120",
        "category": "category-4",
        "name": "butik 4",
        "date": "Sep 4, 2018 15:37:25"
    },
    {
        "id": 5,
        "img": "http://via.placeholder.com/120x120",
        "category": "category-2",
        "name": "butik 5",
        "date": "Sep 4, 2018 15:37:25"
    },
    {
        "id": 6,
        "img": "http://via.placeholder.com/120x120",
        "category": "category-1",
        "name": "butik 6",
        "date": "Sep 4, 2018 15:37:25"
    }
];

router.route('/')
    .get(function (request, response) {
        response.json(boutiques);
    });

module.exports = router;