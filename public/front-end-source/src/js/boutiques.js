// get-boutique
$.ajax('/boutiques', {
    contentType: 'application/json',
    dataType: 'json',
    success: function(response) {
        var boutiqueElements = $.map(response, function(boutique, i){
            var boutiqueItem = $('<div class="box box-product" data-category="'+boutique.category +'"></div>');
            boutiqueItem.data('category', boutique.category);
            $('<h3>'+boutique.name+'</h3>').appendTo(boutiqueItem);
            $('<img>').attr('src', boutique.img).appendTo(boutiqueItem);
            $('<p>'+boutique.date+'</p>').appendTo(boutiqueItem);
            return boutiqueItem;
        });

        // add elements to the corresponding layout areas
        $(".box-container-1").append(boutiqueElements[0]);
        $(".box-container-2").append(boutiqueElements[1]);
        $(".box-container-3").append(boutiqueElements[2]);
        $(".box-container-4").append(boutiqueElements[3]);
        $(".box-container-5").append(boutiqueElements[4]);
        $(".box-container-6").append(boutiqueElements[5]);
    }
});


